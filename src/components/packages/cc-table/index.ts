/*
 * @Author: Devin
 * @Date: 2022-04-01 17:32:42
 * @LastEditors: Devin
 * @LastEditTime: 2022-04-01 17:34:05
 */
import Main from './src/Main.vue';

Main.install = function (Vue: any) {
  Vue.component(Main.name, Main)
};

export default Main