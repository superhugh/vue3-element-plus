/*
 * @Description: 
 * @Author: Devin-chen
 * @Date: 2021-10-25 22:25:04
 * @LastEditTime: 2022-03-17 15:06:39
 * @LastEditors: Devin
 * @Reference: 原创
 */

import Main from './src/Main.vue';

Main.install = function (Vue: any) {
  Vue.component(Main.name, Main)
};

export default Main