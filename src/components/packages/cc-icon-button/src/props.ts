/*
 * @Author: Devin
 * @Date: 2022-05-06 10:42:03
 * @LastEditors: Devin
 * @LastEditTime: 2022-05-06 17:16:13
 */
export default {
  icon: String,
  fantClass: {
    type: String,
    default: 'iconfont'
  },
  disabled: Boolean,
  height: { type: String, default: '26px' },
  width: { type: String, default: '26px' },
  color: { type: String, default: 'var(--cc-main-color)' },
  otherStyle: Object,
  showTip: Boolean,
  onClick: Function,
  title: String,
  toolTip: {
    type: Object,
    default: () => {
      return {
        effect: 'light',
        placement: 'top',
        enterable: false
      };
    }
  }
};
