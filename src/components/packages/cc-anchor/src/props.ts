/*
 * @Author: Devin
 * @Date: 2022-04-11 17:47:51
 * @LastEditors: Devin
 * @LastEditTime: 2022-04-24 18:41:31
 */
export default {
  sections: { type: Array, default: () => [] },
  target: {
    type: String,
    default: '.layout-main-container-main-div'
  },
  duration: { type: Number, default: 50 },
  bound: { type: Number, default: 12 },
  showRail: { type: Boolean, default: true },
  showBackground: { type: Boolean, default: true },
  offset: { type: Number, default: 0 },
  position: { type: String, default: 'top' },
  zIndex: { type: Number, default: 100 },
  width: { type: String, default: '100%' }
};
