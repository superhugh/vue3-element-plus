/*
 * @Description: 
 * @Date: 2022-01-29 13:51:34
 * @LastEditTime: 2022-02-12 16:47:20
 * @FilePath: /vue3-element-plus/src/components/packages/cc-input-number/src/props.ts
 * @Author: Devin
 */
import config from "@/utils/packages-configs/cc-input-number"

const props = {
  modelValue: String,
  size: {
    type: String,
    default: config.size
  },
  prefixIcon: {
    type: String,
    default: config.prefixIcon
  },
  suffixIcon: {
    type: String,
    default: config.suffixIcon
  },
  controlsPosition: {
    type: String,
    default: config.controlsPosition
  },
  numberClass: {
    type: Array,
    default: () => config.numberClass
  }
}
export default props