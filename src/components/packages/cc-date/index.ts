/*
 * @Author: Devin
 * @Date: 2022-03-14 16:38:39
 * @LastEditors: Devin
 * @LastEditTime: 2022-03-17 15:06:35
 */
import Main from './src/Main.vue';

Main.install = function (Vue: any) {
  Vue.component(Main.name, Main)
};

export default Main