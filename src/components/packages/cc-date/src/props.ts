/*
 * @Author: Devin
 * @Date: 2022-03-14 16:42:35
 * @LastEditors: Devin
 * @LastEditTime: 2022-03-15 17:56:46
 */
import config from '@/utils/packages-configs/cc-date';
export default {
  modelValue: [String, Array],
  size: {
    type: String,
    default: config.size
  },
  type: {
    type: String,
    default: config.type
  },
  placeholder: {
    type: String,
    default: config.placeholder
  },
  connector: {
    // 日期中间的链接符
    type: String,
    default: config.connector
  },
  connectorDaytoHour: {
    type: String, // 日期与时间中间的链接符
    default: config.connectorDaytoHour
  },
  canSelectDate: {
    type: [Object, String]
  },
  canDay: {
    type: Boolean
  }
};
