/*
 * @Description:
 * @Author: Devin-chen
 * @Date: 2021-10-22 21:44:08
 * @LastEditTime: 2022-02-17 15:35:20
 * @LastEditors: Please set LastEditors
 * @Reference:
 */
import { VNode, RendererNode, RendererElement } from '@vue/runtime-core';
import { ElMessageBox, MessageBoxData } from 'element-plus';
type MessageType = '' | 'success' | 'warning' | 'info' | 'error';
export interface Confirm {
  title?: string | undefined;
  message?: string | VNode<RendererNode, RendererElement, { [key: string]: any }> | undefined;
  confirmButtonText?: string | undefined;
  cancelButtonText?: string | undefined;
  type?: MessageType | undefined;
  center?: boolean | undefined;
  confirmButtonClass?: string | undefined;
  cancelButtonClass?: string | undefined;
  closeOnClickModal?: boolean | undefined;
}


/**
 * @description:
 * @return {*}
 * @Date: 2021-10-22 22:54:52
 * @author: Devin-chen
 * @param {Function} callbackConfirm 确定时触发的回调函数
 * @param {Function} callbackCancel 取消时触发的回调函数
 */
export function ccMessageConfirm(
  {
    title = '提示',
    message = '是否继续？',
    confirmButtonText = '确定',
    cancelButtonText = '取消',
    type = 'warning',
    center = false,
    confirmButtonClass = 'confirmButtonClass',
    cancelButtonClass = 'cancelButtonClass',
    closeOnClickModal = false
  }: Confirm,
  callbackConfirm: Function = (): void => {},
  callbackCancel: Function = (): void => {}
): Promise<MessageBoxData> {
  return ElMessageBox.confirm(message, title, {
    confirmButtonText: confirmButtonText,
    cancelButtonText: cancelButtonText,
    type: type,
    center: center,
    callback: (action: string) => {
      if (action === 'cancel') {
        callbackCancel();
      }
      if (action === 'confirm') {
        callbackConfirm();
      }
    },
    confirmButtonClass: confirmButtonClass,
    cancelButtonClass: cancelButtonClass,
    closeOnClickModal: closeOnClickModal
  });
}

export interface Alert {
  title?: string | undefined;
  message?: string | VNode<RendererNode, RendererElement, { [key: string]: any }> | undefined;
  confirmButtonText?: string | undefined;
  type?: MessageType | undefined;
  center?: boolean | undefined;
  confirmButtonClass?: string | undefined;
  showClose?: boolean | undefined;
}

/**
 * @description:
 * @return {*}
 * @Date: 2021-10-22 22:54:52
 * @author: Devin-chen
 * @param {Function} callbackConfirm 确定时触发的回调函数
 * @param {Function} callbackCancel 取消时触发的回调函数
 */
export function ccMessageAlert(
  {
    title = '提示',
    message = '是否继续？',
    confirmButtonText = '确定',
    type = 'warning',
    center = false,
    confirmButtonClass = 'confirmButtonClass',
    showClose = false
  }: Alert,
  callbackConfirm: Function = (): void => {},
  callbackCancel: Function = (): void => {}
): Promise<MessageBoxData> {
  return ElMessageBox.alert(message, title, {
    confirmButtonText: confirmButtonText,
    type: type,
    center: center,
    callback: (action: any) => {
      if (action === 'cancel') {
        callbackCancel();
      }
      if (action === 'confirm') {
        callbackConfirm();
      }
    },
    confirmButtonClass: confirmButtonClass,
    showClose: showClose
  });
}
