/*
 * @Author: Devin
 * @Date: 2022-04-24 16:06:53
 * @LastEditors: Devin
 * @LastEditTime: 2022-04-24 16:07:06
 */
import { Loading } from './src/service'
import { vLoading } from './src/directive'

import type { App } from 'vue'

// installer and everything in all
export const ElLoading = {
  install(app: App) {
    app.directive('loading', vLoading)
    app.config.globalProperties.$loading = Loading
  },
  directive: vLoading,
  service: Loading,
}

export default ElLoading
export { vLoading, vLoading as ElLoadingDirective, Loading as ElLoadingService }

