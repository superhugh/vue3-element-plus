/*
 * @Author: Devin
 * @Date: 2022-04-22 15:11:25
 * @LastEditors: Devin
 * @LastEditTime: 2022-04-25 16:54:50
 */
export const tableList = [
  {
    attribute: 'sections',
    info: '渲染锚点列表的数据，有两个属性，label：代表锚点的标题、id：代表锚点的id，若id没有传，则默认取label作为id',
    type: 'array',
    value: '--',
    default: '[ ]'
  },
  {
    attribute: 'target',
    info: '滚动的容器',
    type: 'string',
    value: '--',
    default: '.layout-main-container-main-div'
  },
  {
    attribute: 'bound',
    info: '元素开始触发 anchor 的偏移量',
    type: 'number',
    value: '--',
    default: '12'
  },
  {
    attribute: 'showRail',
    info: '是否展示侧面的轨道',
    type: 'boolean',
    value: 'true | false',
    default: 'true'
  },
  {
    attribute: 'showBackground',
    info: '是否展示锚点列表的背景',
    type: 'boolean',
    value: 'true | false',
    default: 'true'
  },
  {
    attribute: 'width',
    info: '锚点列表的宽度',
    type: 'string',
    value: '--',
    default: '100%'
  },
  {
    attribute: 'offset',
    info: '偏移距离',
    type: 'number',
    value: '--',
    default: '0'
  },
  {
    attribute: 'position',
    info: '位置',
    type: 'string',
    value: 'top | bottom',
    default: 'top'
  },
  {
    attribute: 'z-index',
    info: 'z-index',
    type: 'number',
    value: '--',
    default: '100'
  }
];
export const tableListFun = [
  {
    name: 'scrollTo',
    info: '手动触发到指定滚动位置',
    cs: '(id: string) => void'
  }
];
