/*
 * @Description:
 * @Date: 2021-10-22 09:39:41
 * @LastEditTime: 2022-04-25 11:18:28
 * @FilePath: /vue3-element-plus/src/main.ts
 * @Author: Devin
 */
import { createApp } from 'vue';
import App from './App.vue';
import './shims-vue-d';

import { configGlobalPropertiesFun } from '@/utils/vue/vuePrototype';
import { configErrorHandlerFun } from './utils/vue/vueErrorHandler';
import { configWarnHandlerFun } from './utils/vue/vueWangHandler';
import { vueUseFun } from './utils/vue/vueUse';
import { initThemeColor } from './utils/tools';

/* 在 Vue 3 中，改变全局 Vue 行为的 API 现在被移动到了由新的 createApp 方法所创建的应用实例上。此外，现在它们的影响仅限于该特定应用实例： */
const app = createApp(App);
// 安装 Vue.js 插件。如果插件是一个对象，它必须暴露一个 install 方法。如果它本身是一个函数，它将被视为安装方法。
vueUseFun(app);
// 所提供 DOM 元素的 innerHTML 将被替换为应用根组件的模板渲染结果。
// 需要在最后


// 添加一个可以在应用的任何组件实例中访问的全局 property。
configGlobalPropertiesFun(app);

// 指定一个处理函数，来处理组件渲染方法和侦听器执行期间抛出的未捕获错误。这个处理函数被调用时，可获取错误信息和应用实例。
configErrorHandlerFun(app);

// 为 Vue 的运行时警告指定一个自定义处理函数。注意这只会在开发环境下生效，在生产环境下它会被忽略。
configWarnHandlerFun(app);

// 初始化主题色
initThemeColor();

app.mount('#app');

