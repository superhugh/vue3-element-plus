/*
 * @Description:
 * @Author: Devin-chen
 * @Date: 2021-10-22 19:11:30
 * @LastEditTime: 2022-04-27 15:08:26
 * @LastEditors: Devin
 * @Reference:
 */
import service from '@/utils/service';
const serviceApi = service as any;
export const getInputAutoList = ({ data, url }: { data: object | undefined; url: string }) => {
  return serviceApi({ url, method: 'get', data });
};

export const login = (data: object) => {
  return serviceApi({ url: '/login', method: 'post', data, needLoading: true });
};

export const getSelectList = ({ data, url }: { data: object | undefined; url: string }) => {
  return serviceApi({
    url,
    method: 'get',
    data
  });
};

export const getTableList = ({
  data,
  method,
  url,
  headers
}: {
  data: object | undefined;
  url: string;
  method: string;
  headers: any;
}) => {
  return serviceApi({
    url,
    method,
    data,
    headers
  });
};
