/*
 * @Author: Devin
 * @Date: 2022-03-21 14:57:09
 * @LastEditors: Devin
 * @LastEditTime: 2022-04-28 13:59:01
 */
import Layout from '@/layout/index.vue';

const componentsRoutes = {
  path: '/other',
  name: 'other',
  component: Layout,
  redirect: '/other/anchor',
  meta: {
    title: '其他组件',
    icon: 'icon-modular'
  },
  children: [
    {
      path: 'anchor',
      name: 'Anchor',
      component: () => import('@/views/components/anchor/index.vue'),
      meta: {
        title: '锚点'
      }
    },
    {
      path: 'anchor',
      name: 'anchor',
      component: () => import('@/views/other/anchor/index.vue'),
      meta: {
        title: '数字滚动'
      }
    },
  ]
};

export default componentsRoutes;