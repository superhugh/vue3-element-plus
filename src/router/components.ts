/*
 * @Description:
 * @Date: 2021-09-22 09:29:14
 * @LastEditTime: 2022-04-28 13:58:57
 * @FilePath: /vue3-element-plus/src/router/components.ts
 * @Author: Devin
 */
import Layout from '@/layout/index.vue';

const componentsRoutes = {
  path: '/components',
  name: 'Components',
  component: Layout,
  redirect: '/components/table',
  meta: {
    title: 'Element组件',
    icon: 'icon-zujian'
  },
  children: [
    
    {
      path: 'table',
      name: 'Table',
      component: () => import('@/views/components/table/index.vue'),
      meta: {
        title: '表格'
      }
    },
    {
      path: 'icon-btn',
      name: 'IconBtn',
      component: () => import('@/views/components/icon_btn/index.vue'),
      meta: {
        title: '图标按钮'
      }
    },
    {
      path: 'button',
      name: 'Button',
      component: () => import('@/views/components/button/index.vue'),
      meta: {
        title: '按钮'
      }
    },
    {
      path: 'input',
      name: 'Input',
      component: () => import('@/views/components/input/index.vue'),
      meta: {
        title: '输入框'
      }
    },
    {
      path: 'autocomplete',
      name: 'Autocomplete',
      component: () => import('@/views/components/autocomplete/index.vue'),
      meta: {
        title: '输入框自动补全'
      }
    },
    {
      path: 'input-numer',
      name: 'InputNumber',
      component: () => import('@/views/components/input-number/index.vue'),
      meta: {
        title: '数字输入框'
      }
    },
    {
      path: 'select',
      name: 'Select',
      component: () => import('@/views/components/select/index.vue'),
      meta: {
        title: '选择器'
      }
    },
    {
      path: 'date-select',
      name: 'DateSelect',
      component: () => import('@/views/components/date_select/index.vue'),
      meta: {
        title: '时间选择器'
      }
    },
    {
      path: 'message-box',
      name: 'MessageBox',
      component: () => import('@/views/components/message_box/index.vue'),
      meta: {
        title: '消息弹出框'
      }
    },
    {
      path: 'switch',
      name: 'Switch',
      component: () => import('@/views/components/switch/index.vue'),
      meta: {
        title: '开关'
      }
    },
    {
      path: 'form',
      name: 'Form',
      component: () => import('@/views/components/form/index.vue'),
      meta: {
        title: '表单'
      }
    },
    {
      path: 'dialog',
      name: 'Dialog',
      component: () => import('@/views/components/dialog/index.vue'),
      meta: {
        title: '对话框'
      }
    },
    {
      path: 'upload',
      name: 'Upload',
      component: () => import('@/views/components//uploadCopperImage/index.vue'),
      meta: {
        title: '裁剪上传'
      }
    }
  ]
};

export default componentsRoutes;
