/*
 * @Description:
 * @Date: 2021-10-22 13:12:04
 * @LastEditTime: 2021-11-06 15:15:41
 * @FilePath: \vue3-element-plus\src\store\index.ts
 * @Author: Devin
 */

import { createStore } from 'vuex';
import getters from './getters';
import app from './modules/app';
import sidebar from './modules/sidebar';

const store = createStore({
  modules: {
    app,
    sidebar
  },
  getters
});

export default store;
