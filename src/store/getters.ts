/*
 * @Description:
 * @Date: 2021-10-27 16:49:45
 * @LastEditTime: 2021-11-06 17:16:19
 * @FilePath: \vue3-element-plus\src\store\getters.ts
 * @Author: Devin
 */

const getters = {
  sidebar: (state: { sidebar: { sidebar: any; }; }) => state.sidebar.sidebar,
}

export default getters;
