/*
 * @Description:
 * @Date: 2021-10-27 14:38:31
 * @LastEditTime: 2021-10-27 17:45:57
 * @FilePath: \vue3-element-plus\src\store\modules\app.ts
 * @Author: Devin
 */

function state() {
  return {};
}

const mutations = {};

const getters = {};

const actions = {};

export default {
  state,
  mutations,
  getters,
  actions
};
