/*
 * @Description:
 * @Date: 2021-10-22 11:45:55
 * @LastEditTime: 2022-02-16 16:07:29
 * @FilePath: /vue3-element-plus/src/utils/vue/vuePrototype.ts
 * @Author: Devin
 */

import { ccMessage, CcMessage } from "@/components/packages/cc-message"
import { ccMessageAlert, ccMessageConfirm, Confirm, Alert } from "@/components/packages/cc-message-box";
// 为了告诉 TypeScript 这些新 property，我们可以使用模块扩充
declare module '@vue/runtime-core' {
  export interface ComponentCustomProperties {
    $_message: CcMessage
    $_ccMessageConfirm: Confirm,
    $_ccMessageAlert: Alert
  }
}


export function configGlobalPropertiesFun(app: any): void {
  app.config.globalProperties.$_message = ccMessage;
  app.config.globalProperties.$_ccMessageConfirm = ccMessageConfirm;
  app.config.globalProperties.$_ccMessageAlert = ccMessageAlert;
}

