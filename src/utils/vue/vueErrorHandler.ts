/*
 * @Description:
 * @Date: 2021-10-22 13:20:32
 * @LastEditTime: 2022-02-11 00:44:32
 * @FilePath: \vue3-element-plus\src\utils\vue\vueErrorHandler.ts
 * @Author: Devin
 */
export function configErrorHandlerFun(app: any): void {
	app.config.errorHandler = (err: any, vm: any, info: any) => {
		// 处理错误
		// `info` 是 Vue 特定的错误信息，比如错误所在的生命周期钩子
		console.log(err);
		console.log(vm);
		console.log(info);
	};
}
