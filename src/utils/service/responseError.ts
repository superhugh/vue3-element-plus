/*
 * @Description:
 * @Date: 2021-09-18 14:33:29
 * @LastEditTime: 2022-03-31 18:18:26
 * @FilePath: /vue3-element-plus/src/utils/axios/responseError.ts
 * @Author: Devin
 */
import { ccMessage } from '@/components/packages/cc-message';

function responseError(error: { message: string }) {
  ccMessage.error({ message: error.message });
}
export default responseError;
