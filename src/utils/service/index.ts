/*
 * @Description:
 * @Date: 2022-02-16 18:28:06
 * @LastEditTime: 2022-04-27 16:15:55
 * @FilePath: /vue3-element-plus/src/utils/service/index.ts
 * @Author: Devin
 */
import axios, { AxiosInstance } from 'axios';
import handleConfig from './handleConfig';
import requestError from './requestError';
import responseBusinessError from './responseBusinessError';
import responseError from './responseError';
import { ElLoading } from 'element-plus';
import { showFullScreenLoading, tryHideFullScreenLoading } from './requestLoading';
// 1.创建axios实例
const service: AxiosInstance = axios.create({
  // `baseURL` 将自动加在 `url` 前面，除非 `url` 是一个绝对 URL。
  // 它可以通过设置一个 `baseURL` 便于为 axios 实例的方法传递相对 URL
  baseURL: 'https://www.fastmock.site/mock/ed26be7a6453f01821416b9b7af488dd/devin',

  withCredentials: true, // 表示跨域请求时是否需要使用凭证
  // `timeout` 指定请求超时的毫秒数(0 表示无超时时间)
  // 如果请求话费了超过 `timeout` 的时间，请求将被中断
  timeout: 5000
});
let loadingInstance: any = null;
// 2.添加请求拦截器
service.interceptors.request.use(
  (config: any) => {
    if (config.needLoading) showFullScreenLoading();

    handleConfig(config);
    // 在发送请求之前做些什么
    return config;
  },
  (error) => {
    // 对请求错误做些什么
    requestError(error);
    return Promise.reject(error);
  }
);

// 3.添加响应拦截器
service.interceptors.response.use(
  /**
   * 如果您想获取http信息，如标题或状态
   * 请直接返回 response
   */

  /**
   * 通过自定义代码确定请求状态
   * 这里只是一个例子
   * 您还可以通过HTTP状态代码来判断状态
   */
  (response: any) => {
    const responseData: any = response.data;

    if (response.config.needLoading) tryHideFullScreenLoading();
    // 如果自定义代码不是0，则判断为错误。
    if (responseData.code !== 0) {
      // 这个函数是一些其他的错误码，在这里处理逻辑
      return responseBusinessError(responseData);
    } else {
      return responseData;
    }
  },
  (error) => {
    // 处理不是业务错误的请求错误
    responseError(error);
    return Promise.reject(error);
  }
);

export default service;
