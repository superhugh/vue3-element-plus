/*
 * @Description:
 * @Date: 2022-02-17 15:58:22
 * @LastEditTime: 2022-04-22 14:33:57
 * @FilePath: /vue3-element-plus/src/utils/service/handleConfig.ts
 * @Author: Devin
 */

const handleConfig = (config: any) => {
  config.method = config.method.toLowerCase();
  if (config.method === 'get') {
    config.params = config.data;
    delete config.data;
  }
};
export default handleConfig;
