/*
 * @Description: 
 * @Date: 2022-02-08 11:32:03
 * @LastEditTime: 2022-02-12 17:06:13
 * @FilePath: /vue3-element-plus/src/utils/packages-configs/cc-input-number.ts
 * @Author: Devin
 */
interface InputConfig {
  size: string,
  prefixIcon: string,
  suffixIcon: string,
  controlsPosition: string,
  numberClass: Array<string>
}

let config: InputConfig = {
  size: 'default',
  prefixIcon: '',
  suffixIcon: '',
  controlsPosition: '',
  numberClass: []
}
export default config