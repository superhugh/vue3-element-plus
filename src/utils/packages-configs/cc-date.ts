/*
 * @Author: Devin
 * @Date: 2022-03-14 16:45:34
 * @LastEditors: Devin
 * @LastEditTime: 2022-03-14 17:05:18
 */
interface ccDate {
  size: string;
  type: string;
  placeholder: string;
  connector: string;
  connectorDaytoHour: string;
}

let config: ccDate = {
  size: 'default',
  type: 'date',
  placeholder: '请选择',
  connector: '-',
  connectorDaytoHour: ' '
};

export default config;
