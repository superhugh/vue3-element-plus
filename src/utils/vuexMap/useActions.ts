/*
 * @Description:
 * @Author: Devin
 * @Date: 2021-11-06 15:54:12
 * @LastEditTime: 2021-11-06 17:13:41
 * @LastEditors: Devin
 * @Reference:
 */
import { mapActions, createNamespacedHelpers } from 'vuex';
import { useActionMapper } from './useMapper';
/**
 * @description 封装vuex的mapActions，以便在setup中使用
 * @param {*} moduleName 模块名称
 * @param {*} mapper getters属性集合 ['name', 'age']
 * @return {*}
 */
export function useActions({
  moduleName,
  mapper
}: {
  moduleName?: string;
  mapper: string | string[];
}): any {
  let mapperFn = mapActions;
  if (moduleName) {
    // 如果使用模块化，则使用vuex提供的createNamespacedHelpers方法找到对应模块的mapActions方法
    if (Object.prototype.toString.call(moduleName) === '[object String]' && moduleName.length > 0) {
      mapperFn = createNamespacedHelpers(moduleName).mapActions;
    }
  }

  return useActionMapper(mapper, mapperFn);
}
