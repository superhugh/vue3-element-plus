/*
 * @Author: Devin
 * @Date: 2022-03-30 16:44:55
 * @LastEditors: Devin
 * @LastEditTime: 2022-03-30 17:38:39
 */
export default {
  code:`<pre class=" language-markup" contenteditable="false" data-mce-selected="1"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcForm</span> <span class="token attr-name">:forms</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>forms<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span> <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>CcForm</span><span class="token punctuation">&gt;</span></span></pre>
  <pre class=" language-javascript" contenteditable="false" data-mce-selected="1"><span class="token operator">&lt;</span>script lang<span class="token operator">=</span><span class="token string">"ts"</span><span class="token operator">&gt;</span>
<span class="token keyword">import</span> <span class="token punctuation">{</span> defineComponent<span class="token punctuation">,</span> reactive<span class="token punctuation">,</span> toRefs <span class="token punctuation">}</span> <span class="token keyword">from</span> <span class="token string">'@vue/runtime-core'</span><span class="token punctuation">;</span>
<span class="token keyword">export</span> <span class="token keyword">default</span> <span class="token function">defineComponent</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
  <span class="token function">setup</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token keyword">const</span> state <span class="token operator">=</span> <span class="token function">reactive</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
      forms<span class="token operator">:</span> <span class="token punctuation">{</span>
        input<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'输入框'</span><span class="token punctuation">,</span>
          rules<span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">{</span> required<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span> message<span class="token operator">:</span> <span class="token string">'必填'</span><span class="token punctuation">,</span> trigger<span class="token operator">:</span> <span class="token string">'blur'</span> <span class="token punctuation">}</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span>
          on<span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token function-variable function">input</span><span class="token operator">:</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
              ccMessage<span class="token punctuation">.</span><span class="token function">success</span><span class="token punctuation">(</span><span class="token string">'输入事件'</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
            <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token function-variable function">blur</span><span class="token operator">:</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
              ccMessage<span class="token punctuation">.</span><span class="token function">success</span><span class="token punctuation">(</span><span class="token string">'失焦事件'</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
            <span class="token punctuation">}</span>
          <span class="token punctuation">}</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        date<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'日期选择器'</span><span class="token punctuation">,</span>
          type<span class="token operator">:</span> <span class="token string">'date'</span><span class="token punctuation">,</span>
          rules<span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">{</span> required<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span> message<span class="token operator">:</span> <span class="token string">'必填'</span><span class="token punctuation">,</span> trigger<span class="token operator">:</span> <span class="token string">'blur'</span> <span class="token punctuation">}</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        textarea<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'多行文本'</span><span class="token punctuation">,</span>
          type<span class="token operator">:</span> <span class="token string">'textarea'</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        select<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'选择器'</span><span class="token punctuation">,</span>
          type<span class="token operator">:</span> <span class="token string">'select'</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span>
          url<span class="token operator">:</span> <span class="token string">'/select/list'</span><span class="token punctuation">,</span>
          optionsKey<span class="token operator">:</span> <span class="token punctuation">{</span> value<span class="token operator">:</span> <span class="token string">'id'</span><span class="token punctuation">,</span> label<span class="token operator">:</span> <span class="token string">'name'</span> <span class="token punctuation">}</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        number<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'数字'</span><span class="token punctuation">,</span>
          type<span class="token operator">:</span> <span class="token string">'number'</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span>
        <span class="token punctuation">}</span>
      <span class="token punctuation">}</span>
    <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>

    <span class="token keyword">return</span> <span class="token punctuation">{</span> <span class="token operator">...</span><span class="token function">toRefs</span><span class="token punctuation">(</span>state<span class="token punctuation">)</span> <span class="token punctuation">}</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token operator">&lt;</span><span class="token operator">/</span>script<span class="token operator">&gt;</span></pre>`,
  code1:`<pre class="language-markup" contenteditable="false"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcForm</span> <span class="token attr-name">:forms</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>forms<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>template</span> <span class="token attr-name">#slotItemName</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{ params, form }<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>el-form-item</span> <span class="token attr-name">:rules</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>[{ required: true, message: <span class="token punctuation">'</span>必填<span class="token punctuation">'</span>, trigger: <span class="token punctuation">'</span>blur<span class="token punctuation">'</span> }]<span class="token punctuation">"</span></span> <span class="token attr-name">prop</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>slot<span class="token punctuation">"</span></span> <span class="token attr-name">:label</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>form.label<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcInput</span> <span class="token attr-name">v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>params.slot<span class="token punctuation">"</span></span> <span class="token attr-name">placeholder</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>请输入<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>CcInput</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>el-form-item</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>template</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>template</span> <span class="token attr-name">#slotLabelName</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{ label }<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span class="token attr-name">style</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>color: red<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>{{ label }}<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>span</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>template</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>template</span> <span class="token attr-name">#slotErrorName</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{ error }<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span> <span class="token attr-name">style</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>color: blue; position: absolute; top: 100%; padding-top: 2px; font-size: 12px; line-height: 1<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>{{
      error
    }}<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>span</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>template</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>template</span> <span class="token attr-name">#slotName</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>box<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>span</span><span class="token punctuation">&gt;</span></span>这里可以自定义content内容，有两个参数，params，form ; #slotName="{ params, form }"<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>span</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>template</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>template</span> <span class="token attr-name">#slotItemNamelist</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{ params }<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>div</span> <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>flex-to-center<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>b</span><span class="token punctuation">&gt;</span></span>动态表单（添加/删除表单项）<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>b</span><span class="token punctuation">&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcButton</span> <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>text<span class="token punctuation">"</span></span> <span class="token attr-name">@click</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>add<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>添加<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>CcButton</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>div</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>template</span> <span class="token attr-name">v-for</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>(item, index) in params.list<span class="token punctuation">"</span></span> <span class="token attr-name">:key</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>index + item<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>el-row</span> <span class="token attr-name">:gutter</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>20<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>el-col</span> <span class="token attr-name">:span</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>8<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
          <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>el-form-item</span>
            <span class="token attr-name">:label</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span><span class="token punctuation">'</span>list - <span class="token punctuation">'</span> + (index + 1)<span class="token punctuation">"</span></span>
            <span class="token attr-name">:prop</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span><span class="token punctuation">'</span>list.<span class="token punctuation">'</span> + index + <span class="token punctuation">'</span>.name<span class="token punctuation">'</span><span class="token punctuation">"</span></span>
            <span class="token attr-name">:rules</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{
              required: true,
              message: <span class="token punctuation">'</span>必填<span class="token punctuation">'</span> + index,
              trigger: <span class="token punctuation">'</span>blur<span class="token punctuation">'</span>
            }<span class="token punctuation">"</span></span>
          <span class="token punctuation">&gt;</span></span>
            <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcInput</span> <span class="token attr-name">v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>item.name<span class="token punctuation">"</span></span> <span class="token punctuation">/&gt;</span></span>
          <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>el-form-item</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>el-col</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>el-col</span> <span class="token attr-name">:span</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>8<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
          <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>el-form-item</span>
            <span class="token attr-name">:label</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span><span class="token punctuation">'</span>list - <span class="token punctuation">'</span> + (index + 1)<span class="token punctuation">"</span></span>
            <span class="token attr-name">:prop</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span><span class="token punctuation">'</span>list.<span class="token punctuation">'</span> + index + <span class="token punctuation">'</span>.age<span class="token punctuation">'</span><span class="token punctuation">"</span></span>
            <span class="token attr-name">:rules</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{
              required: true,
              message: <span class="token punctuation">'</span>必选<span class="token punctuation">'</span> + index,
              trigger: <span class="token punctuation">'</span>change<span class="token punctuation">'</span>
            }<span class="token punctuation">"</span></span>
          <span class="token punctuation">&gt;</span></span>
            <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcSelect</span>
              <span class="token attr-name">v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>item.age<span class="token punctuation">"</span></span>
              <span class="token attr-name">style</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>width: 100%<span class="token punctuation">"</span></span>
              <span class="token attr-name">:optionsKey</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>{ value: <span class="token punctuation">'</span>id<span class="token punctuation">'</span>, label: <span class="token punctuation">'</span>name<span class="token punctuation">'</span> }<span class="token punctuation">"</span></span>
              <span class="token attr-name">url</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>/select/list<span class="token punctuation">"</span></span>
            <span class="token punctuation">/&gt;</span></span>
          <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>el-form-item</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>el-col</span><span class="token punctuation">&gt;</span></span>
        <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>el-col</span> <span class="token attr-name">:span</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>8<span class="token punctuation">"</span></span><span class="token punctuation">&gt;</span></span>
          <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcButton</span>
            <span class="token attr-name">class</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>marginT28<span class="token punctuation">"</span></span>
            <span class="token attr-name">type</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>text<span class="token punctuation">"</span></span>
            <span class="token attr-name">:icon</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>Delete<span class="token punctuation">"</span></span>
            <span class="token attr-name">@click</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>delItem(index)<span class="token punctuation">"</span></span>
            <span class="token attr-name">v-if</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>params.list.length !== 1<span class="token punctuation">"</span></span>
        <span class="token punctuation">/&gt;</span></span><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>el-col</span><span class="token punctuation">&gt;</span></span>
      <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>el-row</span><span class="token punctuation">&gt;</span></span>
    <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>template</span><span class="token punctuation">&gt;</span></span>
  <span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>template</span><span class="token punctuation">&gt;</span></span>
<span class="token tag"><span class="token tag"><span class="token punctuation">&lt;/</span>CcForm</span><span class="token punctuation">&gt;</span></span></pre>
<pre class=" language-javascript" contenteditable="false" data-mce-selected="1"><span class="token operator">&lt;</span>script lang<span class="token operator">=</span><span class="token string">"ts"</span><span class="token operator">&gt;</span>
<span class="token keyword">import</span> <span class="token punctuation">{</span> ccMessage <span class="token punctuation">}</span> <span class="token keyword">from</span> <span class="token string">'@/components/packages/cc-message'</span><span class="token punctuation">;</span>
<span class="token keyword">import</span> <span class="token punctuation">{</span> defineComponent<span class="token punctuation">,</span> reactive<span class="token punctuation">,</span> ref<span class="token punctuation">,</span> toRefs <span class="token punctuation">}</span> <span class="token keyword">from</span> <span class="token string">'@vue/runtime-core'</span><span class="token punctuation">;</span>
<span class="token keyword">import</span> <span class="token punctuation">{</span> Delete <span class="token punctuation">}</span> <span class="token keyword">from</span> <span class="token string">'@element-plus/icons-vue'</span><span class="token punctuation">;</span>
<span class="token keyword">export</span> <span class="token keyword">default</span> <span class="token function">defineComponent</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
  name<span class="token operator">:</span> <span class="token string">''</span><span class="token punctuation">,</span>
  components<span class="token operator">:</span> <span class="token punctuation">{</span><span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token function">setup</span><span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token punctuation">{</span>
    <span class="token keyword">const</span> state <span class="token operator">=</span> <span class="token function">reactive</span><span class="token punctuation">(</span><span class="token punctuation">{</span>
      forms<span class="token operator">:</span> <span class="token punctuation">{</span>
        input<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'输入框'</span><span class="token punctuation">,</span>
          rules<span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">{</span> required<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span> message<span class="token operator">:</span> <span class="token string">'必填'</span><span class="token punctuation">,</span> trigger<span class="token operator">:</span> <span class="token string">'blur'</span> <span class="token punctuation">}</span><span class="token punctuation">]</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span>
          on<span class="token operator">:</span> <span class="token punctuation">{</span>
            <span class="token function-variable function">input</span><span class="token operator">:</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
              ccMessage<span class="token punctuation">.</span><span class="token function">success</span><span class="token punctuation">(</span><span class="token string">'输入事件'</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
            <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token function-variable function">blur</span><span class="token operator">:</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
              ccMessage<span class="token punctuation">.</span><span class="token function">success</span><span class="token punctuation">(</span><span class="token string">'失焦事件'</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
            <span class="token punctuation">}</span>
          <span class="token punctuation">}</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        textarea<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'多行文本'</span><span class="token punctuation">,</span>
          type<span class="token operator">:</span> <span class="token string">'textarea'</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        select<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'选择器'</span><span class="token punctuation">,</span>
          type<span class="token operator">:</span> <span class="token string">'select'</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span>
          url<span class="token operator">:</span> <span class="token string">'/select/list'</span><span class="token punctuation">,</span>
          optionsKey<span class="token operator">:</span> <span class="token punctuation">{</span> value<span class="token operator">:</span> <span class="token string">'id'</span><span class="token punctuation">,</span> label<span class="token operator">:</span> <span class="token string">'name'</span> <span class="token punctuation">}</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        slot<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'插槽使用el-form-item'</span><span class="token punctuation">,</span>
          slotItemName<span class="token operator">:</span> <span class="token string">'slotItemName'</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        slotLabelName<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'插槽使用label'</span><span class="token punctuation">,</span>
          slotLabelName<span class="token operator">:</span> <span class="token string">'slotLabelName'</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        slotErrorName<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'插槽使用error'</span><span class="token punctuation">,</span>
          slotErrorName<span class="token operator">:</span> <span class="token string">'slotErrorName'</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span><span class="token punctuation">,</span>
          rules<span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">{</span> required<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span> message<span class="token operator">:</span> <span class="token string">'必填'</span><span class="token punctuation">,</span> trigger<span class="token operator">:</span> <span class="token string">'blur'</span> <span class="token punctuation">}</span><span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        slotName<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'插槽使用content'</span><span class="token punctuation">,</span>
          slotName<span class="token operator">:</span> <span class="token string">'slotName'</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">16</span><span class="token punctuation">,</span>
          rules<span class="token operator">:</span> <span class="token punctuation">[</span><span class="token punctuation">{</span> required<span class="token operator">:</span> <span class="token boolean">true</span><span class="token punctuation">,</span> message<span class="token operator">:</span> <span class="token string">'必填'</span><span class="token punctuation">,</span> trigger<span class="token operator">:</span> <span class="token string">'blur'</span> <span class="token punctuation">}</span><span class="token punctuation">]</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        date<span class="token operator">:</span> <span class="token punctuation">{</span>
          label<span class="token operator">:</span> <span class="token string">'日期选择器'</span><span class="token punctuation">,</span>
          type<span class="token operator">:</span> <span class="token string">'date'</span><span class="token punctuation">,</span>
          span<span class="token operator">:</span> <span class="token number">8</span>
        <span class="token punctuation">}</span><span class="token punctuation">,</span>
        list<span class="token operator">:</span> <span class="token punctuation">{</span>
          slotItemName<span class="token operator">:</span> <span class="token string">'slotItemNamelist'</span><span class="token punctuation">,</span>
          value<span class="token operator">:</span> <span class="token punctuation">[</span>
            <span class="token punctuation">{</span> name<span class="token operator">:</span> <span class="token string">''</span><span class="token punctuation">,</span> age<span class="token operator">:</span> <span class="token string">''</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
            <span class="token punctuation">{</span> name<span class="token operator">:</span> <span class="token string">''</span><span class="token punctuation">,</span> age<span class="token operator">:</span> <span class="token string">''</span> <span class="token punctuation">}</span>
          <span class="token punctuation">]</span>
        <span class="token punctuation">}</span>
      <span class="token punctuation">}</span><span class="token punctuation">,</span>
      Delete
    <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> <span class="token function-variable function">delItem</span> <span class="token operator">=</span> <span class="token punctuation">(</span><span class="token parameter">index<span class="token operator">:</span> number</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      state<span class="token punctuation">.</span>forms<span class="token punctuation">.</span>list<span class="token punctuation">.</span>value<span class="token punctuation">.</span><span class="token function">splice</span><span class="token punctuation">(</span>index<span class="token punctuation">,</span> <span class="token number">1</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span><span class="token punctuation">;</span>
    <span class="token keyword">const</span> <span class="token function-variable function">add</span> <span class="token operator">=</span> <span class="token punctuation">(</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
      state<span class="token punctuation">.</span>forms<span class="token punctuation">.</span>list<span class="token punctuation">.</span>value<span class="token punctuation">.</span><span class="token function">push</span><span class="token punctuation">(</span><span class="token punctuation">{</span> age<span class="token operator">:</span> <span class="token string">''</span><span class="token punctuation">,</span> name<span class="token operator">:</span> <span class="token string">''</span> <span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
    <span class="token punctuation">}</span><span class="token punctuation">;</span>

    <span class="token keyword">return</span> <span class="token punctuation">{</span> <span class="token operator">...</span><span class="token function">toRefs</span><span class="token punctuation">(</span>state<span class="token punctuation">)</span><span class="token punctuation">,</span> delItem<span class="token punctuation">,</span> add <span class="token punctuation">}</span><span class="token punctuation">;</span>
  <span class="token punctuation">}</span>
<span class="token punctuation">}</span><span class="token punctuation">)</span><span class="token punctuation">;</span>
<span class="token operator">&lt;</span><span class="token operator">/</span>script<span class="token operator">&gt;</span></pre>`
}