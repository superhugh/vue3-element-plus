/*
 * @Description: 
 * @Date: 2022-01-27 15:17:14
 * @LastEditTime: 2022-03-14 16:34:37
 * @FilePath: /vue3-element-plus/src/views/components/button/code.js
 * @Author: Devin
 */
export default {
	code: `<pre class=" language-markup" contenteditable="false"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcAutocomplete</span>
  <span class="token attr-name">v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>state<span class="token punctuation">"</span></span>
  <span class="token attr-name">:autocompleteList</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>list<span class="token punctuation">"</span></span>
<span class="token punctuation">/&gt;</span></span></pre><pre class=" language-javascript" contenteditable="false">list<span class="token operator">:</span> <span class="token punctuation">[</span>
  <span class="token punctuation">{</span> value<span class="token operator">:</span> <span class="token string">'vue'</span><span class="token punctuation">,</span> link<span class="token operator">:</span> <span class="token string">'https://github.com/vuejs/vue'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span> value<span class="token operator">:</span> <span class="token string">'element'</span><span class="token punctuation">,</span> link<span class="token operator">:</span> <span class="token string">'https://github.com/ElemeFE/element'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span> value<span class="token operator">:</span> <span class="token string">'cooking'</span><span class="token punctuation">,</span> link<span class="token operator">:</span> <span class="token string">'https://github.com/ElemeFE/cooking'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span> value<span class="token operator">:</span> <span class="token string">'mint-ui'</span><span class="token punctuation">,</span> link<span class="token operator">:</span> <span class="token string">'https://github.com/ElemeFE/mint-ui'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span> value<span class="token operator">:</span> <span class="token string">'vuex'</span><span class="token punctuation">,</span> link<span class="token operator">:</span> <span class="token string">'https://github.com/vuejs/vuex'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span> value<span class="token operator">:</span> <span class="token string">'vue-router'</span><span class="token punctuation">,</span> link<span class="token operator">:</span> <span class="token string">'https://github.com/vuejs/vue-router'</span> <span class="token punctuation">}</span><span class="token punctuation">,</span>
  <span class="token punctuation">{</span> value<span class="token operator">:</span> <span class="token string">'babel'</span><span class="token punctuation">,</span> link<span class="token operator">:</span> <span class="token string">'https://github.com/babel/babel'</span> <span class="token punctuation">}</span>
<span class="token punctuation">]</span></pre>`,
code1:`<pre class=" language-markup" contenteditable="false"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcAutocomplete</span>
<span class="token attr-name">&nbsp;&nbsp;v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>state<span class="token punctuation">"</span></span>
<span class="token attr-name">&nbsp;&nbsp;autocompleteType</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>email<span class="token punctuation">"</span></span>
<span class="token punctuation">/&gt;</span></span></pre>`,
code2:`<pre class=" language-markup" contenteditable="false"><span class="token tag"><span class="token tag"><span class="token punctuation">&lt;</span>CcAutocomplete</span>
<span class="token attr-name">v-model</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>state<span class="token punctuation">"</span></span>
<span class="token attr-name">listType</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>remote<span class="token punctuation">"</span></span>
<span class="token attr-name">field</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>name<span class="token punctuation">"</span></span>
<span class="token attr-name">@select</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>handleSelect<span class="token punctuation">"</span></span>
<span class="token attr-name">value-key</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>name<span class="token punctuation">"</span></span>
<span class="token attr-name">:dataHandler</span><span class="token attr-value"><span class="token punctuation attr-equals">=</span><span class="token punctuation">"</span>dataHandler<span class="token punctuation">"</span></span>
<span class="token punctuation">/&gt;</span></span></pre><pre class=" language-javascript" contenteditable="false"><span class="token keyword">const</span> <span class="token function-variable function">dataHandler</span> <span class="token operator">=</span> <span class="token punctuation">(</span><span class="token parameter">item<span class="token operator">:</span> any</span><span class="token punctuation">)</span> <span class="token operator">=&gt;</span> <span class="token punctuation">{</span>
item<span class="token punctuation">.</span>name <span class="token operator">=</span> item<span class="token punctuation">.</span>name <span class="token operator">+</span> <span class="token string">' 名字经过了dataHandler处理'</span><span class="token punctuation">;</span>
<span class="token keyword">return</span> item<span class="token punctuation">;</span>
<span class="token punctuation">}</span><span class="token punctuation">;</span></pre>`
};
